function Logger() {}

Logger.prototype.log = function(message /*...*/) {
    console.log.apply(console, arguments);
};

Logger.prototype.warn = function() {
    console.warn.apply(console, arguments);
};

Logger.prototype.info = function() {
    console.info.apply(console, arguments);
};

Logger.prototype.error = function(swagger) {
    console.error.apply(console, arguments);
};

module.exports = Logger;