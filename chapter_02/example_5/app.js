var Http = require('http'),
    Router = require('router'),
    server,
    router;
    
var router = new Router();

var server = Http.createServer(function(req, res) {
    router(req, res, function(error) {
       if (!error) {
           res.writeHead(404);
       } else {
           // Error handling
           console.error(error.message, error.stack);
           res.writeHead(400);
       }
    });  
});

server.listen('8080', function() {
    console.log('Server listening on port 8080');
});

var counter = 0;

function createMessage(req, res) {
    var id = counter += 1;
    console.log('Creating a message:', id);
    res.writeHead(201, {
        'Content-Type': 'text/plain'
    });
    res.end('Message-' + id);
}

router.post('/message', createMessage);